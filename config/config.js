// APP Backend Server
exports.BACKEND_API = {
  TYPE: 'http',
  HOST: 'localhost',
  PORT: '8081',
};

// UC-BE api
exports.UC_BACKEND_API = {
  TYPE: 'http',
  HOST: 'localhost',
  PORT: '8080',
};

// cas url
exports.CAS_URL = 'https://sso.amsqa.com/cas';

// this node service base url
exports.BASE_URL = '';

// this node service url
exports.SERVICE_URL = 'http://localhost:3005' + this.BASE_URL;

// logger path
exports.LOGGER_PATH = '/tmp/fe/log/';
