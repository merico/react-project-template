import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';
import {AsyncComponent} from '../../components';
import {Demo} from '../../containers';

const AsyncHook = AsyncComponent(() => import('../../hook/components/useHookDemo/UseHookDemo'));
export default class Portal extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Redirect exact from="/" to="/demo" />
          <Route exact path="/demo" component={Demo} />
          <Route exact path="/hook" component={AsyncHook} />
          <Route render={() => <span>404</span>} />
        </Switch>
      </Router>
    );
  }
}
