import React, {useState, useMemo} from 'react';
import Children from './Children';

function UseHookDemo() {
  const [count, setCount] = useState(0);
  const memoizedChildComponent = useMemo(() => <Children />, [count]);
  return (
    <div>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count)}>=</button>
      <div>{memoizedChildComponent}</div>
    </div>
  );
}

export default UseHookDemo;
