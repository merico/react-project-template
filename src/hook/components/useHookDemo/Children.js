import React, {useEffect} from 'react';

function Children() {
  useEffect(() => {});

  return <div>666</div>;
}

export default Children;
