import {combineReducers} from 'redux';
import Demo from '../containers/Demo/reducer';

export default combineReducers({
  demo: Demo,
});
