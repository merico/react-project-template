import {DEMO} from './reducer';

export function changeTextAction() {
  return {
    type: DEMO,
    payload: 'Hello World',
  };
}
