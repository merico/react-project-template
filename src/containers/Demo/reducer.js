export const DEMO = 'DEMO';

const initialState = {
  text: 'Liveramp',
};
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'DEMO':
      return {...state, text: action.payload};
    default:
      return state;
  }
}
