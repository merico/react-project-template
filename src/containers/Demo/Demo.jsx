import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import intl from 'react-intl-universal';
import Style from './index.module.scss';

class Demo extends PureComponent {
  handleJump = () => {
    this.props.history.push('/hook');
  };

  render() {
    const {text} = this.props;
    return (
      <div>
        <div className={Style.demoRoot}>{`${text} - ${intl.get('companyName')}`}</div>
        <input type="button" value="Jump" onClick={this.handleJump} />
      </div>
    );
  }
}

const mapStateToProps = ({demo}) => ({
  text: demo.text,
});
const mapActionToProps = {};
export default connect(mapStateToProps, mapActionToProps)(Demo);
