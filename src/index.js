import React from 'react';
import {Provider} from 'react-redux';
import Store from './redux/store';
import ReactDOM from 'react-dom';
import {App} from './containers';

const renderApp = () => {
  ReactDOM.render(
    <Provider store={Store}>
      <App />
    </Provider>,
    document.getElementById('app')
  );
};
renderApp();

// Enable HMR
if (module.hot) {
  module.hot.accept(App, () => {
    renderApp();
  });
}
