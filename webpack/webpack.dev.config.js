const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const commonConfig = require('./webpack.base.config.js');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const CSS_MODULE_LOADER = {
  loader: 'css-loader',
  options: {
    modules: true,
  },
};
const POST_CSS_LOADER = {
  loader: 'postcss-loader',
  options: {
    plugins: () => [require('autoprefixer')],
  },
};
module.exports = merge(commonConfig, {
  mode: 'development',
  devtool: 'cheap-module-eval-soure-map',
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: 'bundle.js',
    chunkFilename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.module\.(sc|sa|c)ss$/,
        use: ['style-loader', CSS_MODULE_LOADER, POST_CSS_LOADER, 'sass-loader'],
      },
      {
        test: /\.(sc|sa|c)ss$/,
        exclude: /\.module\.(sc|sa|c)ss$/,
        use: ['style-loader', 'css-loader', POST_CSS_LOADER, 'sass-loader'],
      },
      {
        test: /\.less$/,
        use: ['style-loader', CSS_MODULE_LOADER, POST_CSS_LOADER, 'less-loader'],
      },
    ],
  },
  plugins: [
    //开启HMR(热替换功能,替换更新部分,不重载页面！) 相当于在命令行加 --hot
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      ENV: JSON.stringify('development'),
      'process.env': {
        VUEP_BASE_URL: '/',
      },
    }),
    new BundleAnalyzerPlugin({analyzerPort: 3333}),
  ],
  devServer: {
    contentBase: path.resolve(__dirname, '../build'), //  指定访问资源目录
    historyApiFallback: true, //  该选项的作用所有的404都连接到index.html
    disableHostCheck: true, //  绕过主机检查
    inline: true, //  改动后是否自动刷新
    host: 'localhost', //  访问地址
    port: 3000, // 访问端口
    overlay: true, //  出现编译器错误或警告时在浏览器中显示全屏覆盖
    stats: 'errors-only', // 显示捆绑软件中的错误
    compress: true, // 对所有服务启用gzip压缩
    open: true, // 自动打开浏览器
    progress: true, // 显示编译进度
  },
});
