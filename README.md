# NT-Project-Template

### Template Description
>This React project template based on WebPack4. It includes React solution、CSS module solution、Node Server solution、Eslint Module and so on. We can use it to handle most cases of our daily work.

## Eslint
### How to use 'npm run eslint:test'
>This script can help us check the quality of JavaScript code by default (or custom) rules. After checking, we can see checking result in the terminal.

### How to use "eslint:fix"
>This script can help us fix most of code quality problems. But some of them still need your fix manually.

### Why integrating eslint to Template?
>It aims to improve the quality of the FE code and ensure the uniformity of the front-end code style. Here we use Google JavaScript Eslint rules.

## Using Tip
### Managing session at server side
>If you manage seesion at server side and develop locally, you should use 'axios' for requesting Node server, It can let you access session data at server side. But if you want to use other tools such as Fetch or Ajax, Please set relevant configuratio:```{withCredentials : true}```

## New Features
### Generation Tool
>Good News. We created FE project generation tool, it can make work with FE project template Conveniently. If you want to use it, just install it locally by Node ```sudo npm install ramp-fe -g```

### Trouble Shot
>When you begin using it, it could occure one problem that it cannot find module 'handlebars', just run ```sudo npm install handlebars -g```