import {expect} from 'chai';
import reducer, {DEMO} from '../../src/containers/Demo/reducer';

describe('Demo Reducer Tests', function() {
  const defaultState = {
    text: 'Liveramp',
  };
  it('Change Text', function() {
    const action = {
      type: DEMO,
      payload: 'NT Liveramp',
    };
    expect(reducer(defaultState, action).text).eqls('NT Liveramp');
  });
});
