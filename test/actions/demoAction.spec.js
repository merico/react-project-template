import {expect} from 'chai';
import {DEMO} from '../../src/containers/Demo/reducer';
import * as actions from '../../src/containers/Demo/actions';

describe('Demo Container Actions Tests', function() {
  it('Change Text', function() {
    expect(actions.changeTextAction()).to.eql({
      type: DEMO,
      payload: 'Hello World',
    });
  });
});
